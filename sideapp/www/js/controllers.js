/**
 * Offers the module of ionic
 * @author ionic
 */
angular.module('starter.controllers', [])


  /**
   * Controller for the Popup function of the App
   * @param $scope
   * @param $ionicPopup
   * @author Nico Strauch
   */
    .controller('PopUpCtrl', function($scope, $ionicPopup){
  $scope.showAlert = function() {
    var alertPopup = $ionicPopup.alert({
      title: 'Backup successfully',
      subtitle: 'Feel good with this :)'
    });

    alertPopup.then(function(res) {
      console.log('test');
    });
  };
} )

    /**
     * Controller for the main function of the App
     * @param $scope
     * @param $ionicModal
     * @param $timeout
     * @author ionic
     */
    .controller('AppCtrl', function($scope, $ionicModal, $timeout) {

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  // Form data for the login modal
  $scope.loginData = {};

  // Create the login modal that we will use later
  $ionicModal.fromTemplateUrl('templates/login.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  // Triggered in the login modal to close it
  $scope.closeLogin = function() {
    $scope.modal.hide();
  };

  // Open the login modal
  $scope.login = function() {
    $scope.modal.show();
  };

  // Perform the login action when the user submits the login form
  $scope.doLogin = function() {
    console.log('Doing login', $scope.loginData);

    // Simulate a login delay. Remove this and replace with your login
    // code if using a login system
    $timeout(function() {
      $scope.closeLogin();
    }, 1000);
  };
})

    /**
     * Controller for getting the history of the backup via http-request
     * @param $scope
     * @param $http
     * @author Nico Strauch
     */
    .controller('ApiCtrl', function($scope, $http){
  $http.get('http://easydocs.ddns.net:3001/backup/history').then(function(resp){
    $scope.responseApi = resp.data;
    $scope.apiArray = [];
    for (var x in $scope.responseApi){
      $scope.apiArray.push($scope.responseApi[x]);
      }
    })
  })

    /**
     * Controller for getting the user list and their roles form Joomla.
     * @param $scope
     * @$http
     * @ionicPopup
     * @author Nico Strauch
     */
    .controller('UserCtrl', function($scope, $http, $ionicPopup){
  $scope.selectedValue = null;
  $http.get('http://easydocs.ddns.net:3001/users/list').then(function(resp){
    $scope.responseJoomlaUser = resp.data;
    $scope.joomlaUserArray = [];
    for (var x in $scope.responseJoomlaUser){
      $scope.joomlaUserArray.push($scope.responseJoomlaUser[x]);
    }
  });

      /**
       * Function to get the uploaded files of the selected user
       * @function getUserFiles
       * @author Nico Strauch
       */
    $scope.getUserFiles = function(){
      $http.post('http://easydocs.ddns.net:3001/users/userfiles/' +$scope.selectedValue).then(function(resp){
        $scope.responseUserFiles = resp.data;
        $scope.filesArray = [];
        if ($scope.responseUserFiles.message === 'ok'){
          for (var file in $scope.responseUserFiles.files){
            $scope.filesArray.push($scope.responseUserFiles.files[file]);
          }
        }
        else {
          $ionicPopup.alert({title: 'User not found',
                          subtitle:'Please make sure that you tipped an existing user.'})
        }
      })
    };

      /**
       * Function to get the users and roles
       * @function getUserFiles
       * @author Nico Strauch
       */
    $scope.getUserRoles = function(){
      $http.get('http://easydocs.ddns.net:3001/users/list').then(function(resp){
        $scope.responseUserList = resp.data;
        $scope.userArray = [];
        for (var user in $scope.responseUserList){
          $scope.userArray.push($scope.responseUserList[user]);
        }
      })
    }
  })

    /**
     * Controller for the backup functions
     * @param $scope
     * @param $http
     * @param ionicPopup
     * @author Nico Strauch
     */
    .controller('backupCtrl', function($scope, $http, $ionicPopup) {
      $http.get('http://easydocs.ddns.net:3001/backup/config').then(function(resp){
        $scope.responseConfig = resp.data;
        $http.get('http://easydocs.ddns.net:3001/backup/history').then(function(resp){
          $scope.responseApi = resp.data;
          $scope.apiArray = [];
          for (var x in $scope.responseApi){
            $scope.apiArray.push($scope.responseApi[x]);
          }
        })
      });

      /**
       * Function to set an ID to $scope
       * @function setBID
       * @param id
       * @author Nico Strauch
         */
  $scope.setBID = function(id){
    $scope.backupID = id;
  };

      /**
       * Function to restore a backup
       * @function restore
       * @author Nico Strauch
       */
  $scope.restore = function(){
    if ($scope.backupID == null) {
      alert({title:'No Backup selected'})}
    else {
      $http.get('http://easydocs.ddns.net:3001/backup/restore/'+$scope.backupID).then(function(resp){
        $scope.responseRestore = resp.data;
        $ionicPopup.alert({title: 'Restore process started!',
        template:'We hope you do not get in trouble'})
      })
    }
  };

      /**
       * Function to start the backup service
       * @function doBackup
       * @author Nico Strauch
       */
  $scope.doBackup = function(){
    $http.get('http://easydocs.ddns.net:3001/backup/once/start').then(function(resp){
      $scope.responseStart = resp.data;
      $ionicPopup.alert({title: 'Backup started',
        template:'Grab a mug of coffee and enjoy the time.'})
    })
  };

      /**
       * Function to stop the scheduled backup service
       * @function stopScheduleBackup
       * @author Nico Strauch
       */
  $scope.stopScheduleBackup = function() {
    $http.get('http://easydocs.ddns.net:3001/backup/scheduled/stop').then(function(resp){
      $scope.responseStop = resp.data;
    })
  };

      /**
       * Function to start the scheduled backup service
       * @function startScheduleBackup
       * @author Nico Strauch
       */
  $scope.startScheduleBackup = function() {
    $http.get('http://easydocs.ddns.net:3001/backup/scheduled/start').then(function(resp){
      $scope.responseStop = resp.data;
    })
  };

      /**
       * Function to save the configuration of the schedule backup service
       * @function saveConfig
       * @author Nico Strauch
       */
    $scope.saveConfig = function() {
      var url = 'http://easydocs.ddns.net:3001/backup/config';
      $http.post(url, $scope.responseConfig).then(function(resp){
        $scope.responseData = resp.data;
        console.log(resp.data);
        $ionicPopup.alert({title: 'Config saved!'});
      })
    };

});
